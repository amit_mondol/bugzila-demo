package com.amsoft.bugzila.bugzilademo.controller;

import com.amsoft.bugzila.bugzilademo.entity.*;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import org.springframework.ui.Model;
import org.springframework.web.client.RestTemplate;


import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.EnumSet;


@Controller
@RequestMapping("/bug")
public class BugController {
    private static final Logger log = LoggerFactory.getLogger(BugController.class);

    @Autowired
    private RestTemplate restTemplate;


    private final String ID_VAR = "id";
    private final String Product_VAR = "Product";
    private final String Component_VAR = "Component";
    private final String Hardware_VAR = "Hardware";
    private final String Severity_VAR = "Severity";
    private final String Priority_VAR = "Priority";
    private final String Assignee_VAR = "Assignee";
    private final String Status_VAR = "Status";
    private final String Summary_VAR = "Summary";
    private final String Descriptions_VAR = "Descriptions";
    private final String Changed_VAR = "Changed";


    // Searching Parameter
    private String query_format = "advanced";
    private String resolution = "---";
    private String product = "Firefox";
    private String chfieldto = "2020-05-31";
    private String order = "Importance";
    private String bug_status = "NEW";
    private String chfieldfrom = "2019-06-01";
    private String classification = "Client Software";
    private String component = "General";
    private String limit = "10";

    @GetMapping("/")
    public String welcome(Model model) {
        return "Welcome to Bugzila";
    }


    @RequestMapping(value = "/bugs", method = RequestMethod.GET)
    public ResponseEntity<Object> method(HttpServletResponse httpServletResponse) throws IOException {
        String url = "https://bugzilla.mozilla.org/rest/bug?" +
                "query_format=" + query_format +
                "&resolution=" + resolution +
                "&product=" + product +
                "&chfieldto=" + chfieldto +
                "&order=" + order + "" +
                "&bug_status=" + bug_status + "" +
                "&chfieldfrom=" + chfieldfrom +
                "&classification=" + classification + "" +
                "&component=" + component +
                "&limit=" + limit + "";

        //https://bugzilla.mozilla.org/rest/bug?query_format=advanced&resolution=---&product=Firefox&chfieldto=2020-05-31&order=Importance&bug_status=NEW&chfieldfrom=2019-06-01&classification=Client Software&component=General&limit=10
        //https://bugzilla.mozilla.org/rest/bug?query_format=advanced&resolution=---&product=Firefox&chfieldto=2020-05-31&order=Importance&bug_status=NEW&chfieldfrom=2019-06-01&classification=Client
        System.out.println(url);
//			BugList bugList = restTemplate.getForObject(url, BugList.class);
//			log.info(bugList.getBugs().toString());

//            List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
//            MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
//            converter.setSupportedMediaTypes(Collections.singletonList(MediaType.ALL));
//            messageConverters.add(converter);
//            restTemplate.setMessageConverters(messageConverters);

        ResponseEntity<String> bugs_string = restTemplate.getForEntity(url, String.class);
        BugList bugList = restTemplate.getForObject(url, BugList.class);

        System.out.println(bugList.toString());
        bugHistories(bugList);

        System.out.println("Done!");
        return new ResponseEntity<Object>("Fetch successfully! Please check excel file.", HttpStatus.OK);
    }

    private void bugHistories(BugList bugList) throws IOException {
        Workbook workbook = new XSSFWorkbook();

        Sheet sheet = workbook.createSheet("Persons");
        sheet.setColumnWidth(0, 6000);
        sheet.setColumnWidth(1, 4000);

        String excelInputs[] = {ID_VAR, Product_VAR, Component_VAR, Hardware_VAR, Severity_VAR, Priority_VAR, Assignee_VAR, Status_VAR, Summary_VAR, Descriptions_VAR, Changed_VAR};

        Row header = sheet.createRow(0);
        CellStyle headerStyle = workbook.createCellStyle();
        headerStyle.setFillForegroundColor(IndexedColors.LIGHT_YELLOW.getIndex());
        headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

        XSSFFont font = ((XSSFWorkbook) workbook).createFont();
        font.setFontName("Arial");
        font.setFontHeightInPoints((short) 16);
        font.setBold(true);
        headerStyle.setFont(font);


        int col_no = 0;
        for (String excelInput : excelInputs) {
            Cell headerCell = header.createCell(col_no);
            headerCell.setCellValue(excelInput);
            headerCell.setCellStyle(headerStyle);
            col_no++;
        }

//        Arrays.asList(excelInputs).forEach((value) -> {
//        });

        CellStyle style = workbook.createCellStyle();
        style.setWrapText(true);

        int row_no = 2;
        for (Bug bug : bugList.getBugs()) {
            System.out.println(bug.toString());
            BugHistories bugHistories = bugHistory(bug.getID());
            Row row = sheet.createRow(row_no);
            col_no = 0;
            for (String excelInput : excelInputs) {
                Cell cell = row.createCell(col_no);
                cell.setCellStyle(style);
                switch (excelInput) {
                    case ID_VAR:
                        cell.setCellValue(bug.getID());
                        break;
                    case Product_VAR:
                        cell.setCellValue(bug.getProduct());
                        break;
                    case Component_VAR:
                        cell.setCellValue(bug.getComponent());
                        break;
                    case Hardware_VAR:
                        cell.setCellValue(bug.getID());
                        break;
                    case Severity_VAR:
                        cell.setCellValue(bug.getSeverity());
                        break;
                    case Priority_VAR:
                        cell.setCellValue(bug.getPriority());
                        break;
                    case Assignee_VAR:
                        cell.setCellValue(bug.getAssignedTo());
                        break;
                    case Status_VAR:
                        cell.setCellValue(bug.getStatus());
                        break;
                    case Summary_VAR:
                        cell.setCellValue(bug.getSummary());
                        break;
                    case Descriptions_VAR:
                        cell.setCellValue("");
                        break;
                    case Changed_VAR:
                        OffsetDateTime when = null;
                        for (BugHistory _bug : bugHistories.getBugHistories()) {
                            for (History _history : _bug.getHistory()) {
                                OffsetDateTime _when = _history.getWhen();
                                if (when != null) {
                                    if (_when.isAfter(when)) {
                                        when = _when;
                                    }
                                } else {
                                    when = _when;
                                }

                            }
                        }
                        if (when != null) {
                            DateTimeFormatter fmt = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");
                            String dateStr = fmt.format(when);
                            cell.setCellValue(dateStr);
                        } else {
                            cell.setCellValue("");
                        }
                        break;
                    default:
                        cell.setCellValue("");
                        break;
                }
                col_no++;
            }
            row_no++;
        }


        File currDir = new File(".");
        String path = currDir.getAbsolutePath();
        //System.out.println("path: " + path);
        String fileLocation = path.substring(0, path.length() - 1) + "temp.xlsx";

        FileOutputStream outputStream = new FileOutputStream(fileLocation);
        workbook.write(outputStream);
        workbook.close();
    }

    private BugHistories bugHistory(long id) {
        String url = "https://bugzilla.mozilla.org/rest/bug/" + id + "/history";
        System.out.println(url);
        ResponseEntity<String> bugs_string = restTemplate.getForEntity(url, String.class);
        BugHistories bugHistories = restTemplate.getForObject(url, BugHistories.class);
        return bugHistories;
    }

    private void excel(BugList bugList) throws IOException {
        Workbook workbook = new XSSFWorkbook();

        Sheet sheet = workbook.createSheet("Persons");
        sheet.setColumnWidth(0, 6000);
        sheet.setColumnWidth(1, 4000);

        // Header
        Row header = sheet.createRow(0);
        CellStyle headerStyle = workbook.createCellStyle();
        headerStyle.setFillForegroundColor(IndexedColors.LIGHT_BLUE.getIndex());
        headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

        XSSFFont font = ((XSSFWorkbook) workbook).createFont();
        font.setFontName("Arial");
        font.setFontHeightInPoints((short) 16);
        font.setBold(true);
        headerStyle.setFont(font);

        Cell headerCell = header.createCell(0);
        headerCell.setCellValue("Name");
        headerCell.setCellStyle(headerStyle);

        headerCell = header.createCell(1);
        headerCell.setCellValue("Age");
        headerCell.setCellStyle(headerStyle);


        CellStyle style = workbook.createCellStyle();
        style.setWrapText(true);

        Row row = sheet.createRow(2);
        Cell cell = row.createCell(0);
        cell.setCellValue("John Smith");
        cell.setCellStyle(style);

        cell = row.createCell(1);
        cell.setCellValue(20);
        cell.setCellStyle(style);


        File currDir = new File(".");
        String path = currDir.getAbsolutePath();
        System.out.println("path: " + path);
        String fileLocation = path.substring(0, path.length() - 1) + "temp.xlsx";

        FileOutputStream outputStream = new FileOutputStream(fileLocation);
        workbook.write(outputStream);
        workbook.close();
    }

    private void excel_old(BugList bugList) throws IOException {
        Workbook workbook = new XSSFWorkbook();

        Sheet sheet = workbook.createSheet("Persons");
        sheet.setColumnWidth(0, 6000);
        sheet.setColumnWidth(1, 4000);

        // Header
        Row header = sheet.createRow(0);
        CellStyle headerStyle = workbook.createCellStyle();
        headerStyle.setFillForegroundColor(IndexedColors.LIGHT_BLUE.getIndex());
        headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

        XSSFFont font = ((XSSFWorkbook) workbook).createFont();
        font.setFontName("Arial");
        font.setFontHeightInPoints((short) 16);
        font.setBold(true);
        headerStyle.setFont(font);

        Cell headerCell = header.createCell(0);
        headerCell.setCellValue("Name");
        headerCell.setCellStyle(headerStyle);

        headerCell = header.createCell(1);
        headerCell.setCellValue("Age");
        headerCell.setCellStyle(headerStyle);


        CellStyle style = workbook.createCellStyle();
        style.setWrapText(true);

        Row row = sheet.createRow(2);
        Cell cell = row.createCell(0);
        cell.setCellValue("John Smith");
        cell.setCellStyle(style);

        cell = row.createCell(1);
        cell.setCellValue(20);
        cell.setCellStyle(style);


        File currDir = new File(".");
        String path = currDir.getAbsolutePath();
        System.out.println("path: " + path);
        String fileLocation = path.substring(0, path.length() - 1) + "temp.xlsx";

        FileOutputStream outputStream = new FileOutputStream(fileLocation);
        workbook.write(outputStream);
        workbook.close();
    }

//    @RequestMapping(value = "/bugs", method = RequestMethod.GET)
//    public void method(HttpServletResponse httpServletResponse) {
//        String projectUrl = "https://bugzilla.mozilla.org/rest/bug?query_format=advanced&resolution=---&product=Firefox&chfieldto=2020-05-31&order=Importance&bug_status=NEW&chfieldfrom=2019-06-01&classification=Client%20Software&component=General&limit=0";
//        httpServletResponse.setHeader("Location", projectUrl);
//        httpServletResponse.setStatus(302);
//    }

//    @GetMapping("/bugs")
//    @ResponseBody
//    public String getData(Model model) throws IOException {
//        String url = "https://bugzilla.mozilla.org/rest/bug?query_format=advanced&resolution=---&product=Firefox&chfieldto=2020-05-31&order=Importance&bug_status=NEW&chfieldfrom=2014-06-01&classification=Client%20Software&component=General&limit=0";
//
//
////        HttpResponse<JsonNode> jsonResponse = Unirest.get("http://samples.openweathermap.org/data/2.5/weather?q=London,uk&appid=XXXxxxxx")
////                //.routeParam("method", "get")
////                //.queryString("name", "Mark")
////                .asJson();
////
////        System.out.println(jsonResponse.getBody());
//
//        RequestEntity request = RequestEntity
//                .get(new URI("https://example.com/foo"))
//                .accept(MediaType.APPLICATION_JSON);
//        ParameterizedTypeReference<List<Bug>> myBean =
//                new ParameterizedTypeReference<List<MyResponse>>() {};
//        ResponseEntity<List<MyResponse>> response = template.exchange(request, myBean);
//
//
//        return "Hello";
//    }

//    public <T> ResponseEntity<T> exchange(String url,
//                                          HttpMethod method,
//                                          @Nullable
//                                                  HttpEntity<?> requestEntity,
//                                          ParameterizedTypeReference<T> responseType,
//                                          Object... uriVariables)
//            throws RestClientException {
//
//        ParameterizedTypeReference<List<MyBean>> myBean =
//                new ParameterizedTypeReference<List<MyBean>>() {};
//
//        ResponseEntity<List<MyBean>> response = template.exchange("https://example.com",HttpMethod.GET, null, myBean);
//
//    }


    @GetMapping("/list")
    @ResponseBody
    public String loadData(Model model) throws IOException {
        String url = "https://bugzilla.mozilla.org/buglist.cgi?query_format=advanced&resolution=---&product=Firefox&chfieldto=2020-05-31&order=Importance&bug_status=NEW&chfieldfrom=2014-06-01&classification=Client%20Software&component=General&limit=0";

        //  query_format=advanced&
// resolution=---&
// product=Firefox&
// chfieldto=2020-05-31&
// order=Importance&
// bug_status=NEW&
// chfieldfrom=2014-06-01&
// classification=Client%20Software&
// component=General&
// limit=0

//        Document doc = null;
//        try {
//            //https://bugzilla.mozilla.org/buglist.cgi?query_format=advanced&classification=Client%20Software&classification=Developer%20Infrastructure&classification=Components&classification=Server%20Software&classification=Other&short_desc_type=allwordssubstr&resolution=---&order=Importance&product=Bugzilla&short_desc=closed
//            doc = Jsoup.connect(url).get();
//            System.out.println(doc);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        Elements newsHeadlines = doc.select("html body.table");
//        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>.");
//        System.out.println(newsHeadlines);

        //System.out.println(getBugTitleByBugId("314817"));
        getBugList(url);
        //getBugTitleByBugId("314817")
        return "Hello"; //newsHeadlines.html();
    }

    private void getBugList(String url) throws IOException {
        Document doc = Jsoup.connect(url).get();
        System.out.println(doc);
//        Element table = doc.select("table").get(1);
//        Elements body = table.select("tbody");
//        Elements rows = body.select("tr");
//        for (Element row : rows) {
//            System.out.print("111 >>>>>>>  "+ row.select("th").text());
//            System.out.print("111 >>>>>>>  "+ row.select("td").text());
//            System.out.println("\n\n");
//        }

//        ArrayList<String> downServers = new ArrayList<>();
//        Elements tables = doc.select("table");
//
//        Element table = tables.get(0); //select the first table.
//        //Elements rows = table.select("tr");
//        Elements rows = doc.select("table.bz_buglist tr");
//
//
//        ArrayList<Long> bugId = new ArrayList<>();
//        for (Element row : rows) { //first row is the col names so skip it.
//            Elements cols = row.select("td");
//            for (Element col : cols) {
//                //System.out.println("col: " + col.text());
//                bugId.add(Long.parseLong(col.text()));
//                break;
//            }
//            //System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
//        }
//        System.out.println(">>>bugId: " + bugId.size());
//        //Collections.sort(bugId, Collections.reverseOrder());
//        for (Long id : bugId) {
//            System.out.println("id: " + id);
//        }
//        System.out.println(">>>bugId: " + bugId.size());
//        System.out.println("total: " + rows.size());
//        System.out.println("tables: " + tables.size());
    }

    private String getBugTitleByBugId(String bugId) {
        Document doc = null;
        try {
            //https://bugzilla.mozilla.org/buglist.cgi?query_format=advanced&classification=Client%20Software&classification=Developer%20Infrastructure&classification=Components&classification=Server%20Software&classification=Other&short_desc_type=allwordssubstr&resolution=---&order=Importance&product=Bugzilla&short_desc=closed
            doc = Jsoup.connect("https://bugs.eclipse.org/bugs/show_activity.cgi?id=" + bugId).get();
            //System.out.println(doc);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Elements newsHeadlines = doc.select("html body.bugs-eclipse-org-bugs.yui-skin-sam div#titles span#title");
        return newsHeadlines.html();
    }
}

// if (cols.get(7).text().equals("down") && cols.get(3).text().equals("")) {
//         downServers.add(cols.get(5).text());
//         }

//111 >>>>>>>
//
//<td class="first-child bz_id_column"><a class="bz_bug_link
//        bz_status_NEW" title="NEW - Rename `flags` to `loadFlags` in the options object argument passed by browser.loadURI consumers" href="/show_bug.cgi?id=1558145">1558145</a> <span style="display: none"></span> </td>
//<td style="white-space: nowrap" class="bz_bug_type_column" sorttable_customkey="300"> <span class="bug-type-label iconic" title="task" aria-label="task" data-type="task"> <span class="icon" aria-hidden="true"></span> </span> </td>
//<td class="bz_short_desc_column"> <a href="/show_bug.cgi?id=1558145">Rename `flags` to `load<wbr>Flags` in the options object argument passed by browser<wbr>.load<wbr>URI consumers </a> </td>
//<td style="white-space: nowrap" class="bz_product_column"> <span title="Firefox">Firefox </span> </td>
//<td style="white-space: nowrap" class="bz_component_column"> <span title="General">General </span> </td>
//<td style="white-space: nowrap" class="bz_assigned_to_column"> <span title="prem.kumar.krishnan">prem.kumar.krishnan </span> </td>
//<td style="white-space: nowrap" class="bz_bug_status_column" sorttable_customkey="200"> <span title="NEW">NEW </span> </td>
//<td style="white-space: nowrap" class="bz_resolution_column" sorttable_customkey="100"> <span title="---">--- </span> </td>
//<td style="white-space: nowrap" class="bz_changeddate_column" sorttable_customkey="2019-06-13 17:45:20">2019-06-13 </td>
//
//
//
//        111 >>>>>>>  <td class="first-child bz_id_column"><a class="bz_bug_link

//
//<tr id="b1425524" class="bz_bugitem
//        bz_enhancement             bz_normal             bz_P5             bz_NEW                                                    bz_row_odd             ">
//<td class="first-child bz_id_column"><a class="bz_bug_link
//        bz_status_NEW" title="NEW - [meta] move frontend content process JS code to C++ or Rust where feasible" href="/show_bug.cgi?id=1425524">1425524</a> <span style="display: none"></span> </td>
//<td style="white-space: nowrap" class="bz_bug_type_column" sorttable_customkey="200"> <span class="bug-type-label iconic" title="enhancement" aria-label="enhancement" data-type="enhancement"> <span class="icon" aria-hidden="true"></span> </span> </td>
//<td class="bz_short_desc_column"> <a href="/show_bug.cgi?id=1425524">[meta] move frontend content process JS code to C++ or Rust where feasible </a> </td>
//<td style="white-space: nowrap" class="bz_product_column"> <span title="Firefox">Firefox </span> </td>
//<td style="white-space: nowrap" class="bz_component_column"> <span title="General">General </span> </td>
//<td style="white-space: nowrap" class="bz_assigned_to_column"> <span title="nobody">nobody </span> </td>
//<td style="white-space: nowrap" class="bz_bug_status_column" sorttable_customkey="200"> <span title="NEW">NEW </span> </td>
//<td style="white-space: nowrap" class="bz_resolution_column" sorttable_customkey="100"> <span title="---">--- </span> </td>
//<td style="white-space: nowrap" class="bz_changeddate_column" sorttable_customkey="2020-05-13 06:15:07">Tue 23:15 </td>
//</tr>