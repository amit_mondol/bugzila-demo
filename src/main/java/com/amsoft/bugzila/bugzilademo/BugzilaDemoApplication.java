package com.amsoft.bugzila.bugzilademo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class BugzilaDemoApplication {

    private static final Logger log = LoggerFactory.getLogger(BugzilaDemoApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(BugzilaDemoApplication.class, args);
    }

    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        return builder.build();
    }

    @Bean
    public CommandLineRunner run(RestTemplate restTemplate) throws Exception {
        return args -> {
//			String url = "https://gturnquist-quoters.cfapps.io/api/random";
//			Quote quote = restTemplate.getForObject(url, Quote.class);
//			log.info(quote.toString());


//            String url = "https://bugzilla.mozilla.org/rest/bug?query_format=advanced&resolution=---&product=Firefox&chfieldto=2020-05-31&order=Importance&bug_status=NEW&chfieldfrom=2019-06-01&classification=Client Software&component=General&limit=10";
//			BugList bugs = restTemplate.getForObject(url, BugList.class);
//			log.info(bugs.getBugs().toString());
//
//            List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
//            MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
//            converter.setSupportedMediaTypes(Collections.singletonList(MediaType.ALL));
//            messageConverters.add(converter);
//            restTemplate.setMessageConverters(messageConverters);


//            String url = "https://bugzilla.mozilla.org/rest/bug?query_format=advanced&resolution=---&product=Firefox&chfieldto=2020-05-31&order=Importance&bug_status=NEW&chfieldfrom=2019-06-01&classification=Client Software&component=General&limit=10";
//            ResponseEntity<String> bugs_string = restTemplate.getForEntity(url, String.class);
//            BugList bugs = restTemplate.getForObject(url, BugList.class);
//
//            System.out.println(bugs.toString());
//            System.out.println("Done!");
        };
    }
}
