package com.amsoft.bugzila.bugzilademo.entity;


import com.fasterxml.jackson.annotation.*;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Change {
    private String added;
    private String fieldName;
    private String removed;

    @JsonProperty("added")
    public String getAdded() { return added; }
    @JsonProperty("added")
    public void setAdded(String value) { this.added = value; }

    @JsonProperty("field_name")
    public String getFieldName() { return fieldName; }
    @JsonProperty("field_name")
    public void setFieldName(String value) { this.fieldName = value; }

    @JsonProperty("removed")
    public String getRemoved() { return removed; }
    @JsonProperty("removed")
    public void setRemoved(String value) { this.removed = value; }
}