package com.amsoft.bugzila.bugzilademo.entity;

import com.fasterxml.jackson.annotation.*;

import java.util.Arrays;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BugHistory {
    private String alias;
    private History[] history;
    private long id;

    @JsonProperty("alias")
    public String getAlias() { return alias; }
    @JsonProperty("alias")
    public void setAlias(String value) { this.alias = value; }

    @JsonProperty("history")
    public History[] getHistory() { return history; }
    @JsonProperty("history")
    public void setHistory(History[] value) { this.history = value; }

    @JsonProperty("id")
    public long getID() { return id; }
    @JsonProperty("id")
    public void setID(long value) { this.id = value; }

    @Override
    public String toString() {
        return "BugHistory{" +
                "alias=" + alias +
                ", history=" + Arrays.toString(history) +
                ", id=" + id +
                '}';
    }
}