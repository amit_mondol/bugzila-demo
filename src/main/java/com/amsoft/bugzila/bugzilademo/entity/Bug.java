package com.amsoft.bugzila.bugzilademo.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.*;
import java.time.OffsetDateTime;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Bug {
    private Flag[] flags;
    private Detail assignedToDetail;
    private Object[] duplicates;
    private String cfFxIteration;
    private long[] blocks;
    private Object[] mentorsDetail;
    private OffsetDateTime creationTime;
    private String cfTrackingFirefoxRelnote;
    private Object[] mentors;
    private String cfTrackingFirefox78;
    private String platform;
    private String cfCrashSignature;
    private String cfStatusFirefoxEsr68;
    private Object[] regressions;
    private String cfTrackingFirefoxEsr68;
    private Object dupeOf;
    private String cfStatusFirefox78;
    private String cfTrackingFirefox76;
    private String cfHasRegressionRange;
    private String assignedTo;
    private String type;
    private Object[] groups;
    private String priority;
    private String[] cc;
    private OffsetDateTime cfLastResolved;
    private Object cfRank;
    private String[] keywords;
    private long commentCount;
    private String severity;
    private long id;
    private Object[] regressedBy;
    private boolean isOpen;
    private String cfQAWhiteboard;
    private String cfTrackingFirefoxSumo;
    private Object[] dependsOn;
    private String cfTrackingFirefox79;
    private String cfTrackingFirefox77;
    private String status;
    private String url;
    private String targetMilestone;
    private String qaContact;
    private Detail creatorDetail;
    private String cfHasStr;
    private String creator;
    private String cfRootCause;
    private Detail[] ccDetail;
    private String cfStatusFirefox76;
    private String cfWebcompatPriority;
    private String opSys;
    private String cfFxPoints;
    private String resolution;
    private Object[] seeAlso;
    private String cfFissionMilestone;
    private boolean isCreatorAccessible;
    private OffsetDateTime lastChangeTime;
    private String cfStatusFirefox79;
    private String component;
    private String product;
    private String classification;
    private String cfUserStory;
    private long votes;
    private boolean isCcAccessible;
    private String summary;
    private String version;
    private String cfStatusFirefox77;
    private boolean isConfirmed;
    private Object alias;
    private String whiteboard;

    @JsonProperty("flags")
    public Flag[] getFlags() { return flags; }
    @JsonProperty("flags")
    public void setFlags(Flag[] value) { this.flags = value; }

    @JsonProperty("assigned_to_detail")
    public Detail getAssignedToDetail() { return assignedToDetail; }
    @JsonProperty("assigned_to_detail")
    public void setAssignedToDetail(Detail value) { this.assignedToDetail = value; }

    @JsonProperty("duplicates")
    public Object[] getDuplicates() { return duplicates; }
    @JsonProperty("duplicates")
    public void setDuplicates(Object[] value) { this.duplicates = value; }

    @JsonProperty("cf_fx_iteration")
    public String getCFFxIteration() { return cfFxIteration; }
    @JsonProperty("cf_fx_iteration")
    public void setCFFxIteration(String value) { this.cfFxIteration = value; }

    @JsonProperty("blocks")
    public long[] getBlocks() { return blocks; }
    @JsonProperty("blocks")
    public void setBlocks(long[] value) { this.blocks = value; }

    @JsonProperty("mentors_detail")
    public Object[] getMentorsDetail() { return mentorsDetail; }
    @JsonProperty("mentors_detail")
    public void setMentorsDetail(Object[] value) { this.mentorsDetail = value; }

    @JsonProperty("creation_time")
    public OffsetDateTime getCreationTime() { return creationTime; }
    @JsonProperty("creation_time")
    public void setCreationTime(OffsetDateTime value) { this.creationTime = value; }

    @JsonProperty("cf_tracking_firefox_relnote")
    public String getCFTrackingFirefoxRelnote() { return cfTrackingFirefoxRelnote; }
    @JsonProperty("cf_tracking_firefox_relnote")
    public void setCFTrackingFirefoxRelnote(String value) { this.cfTrackingFirefoxRelnote = value; }

    @JsonProperty("mentors")
    public Object[] getMentors() { return mentors; }
    @JsonProperty("mentors")
    public void setMentors(Object[] value) { this.mentors = value; }

    @JsonProperty("cf_tracking_firefox78")
    public String getCFTrackingFirefox78() { return cfTrackingFirefox78; }
    @JsonProperty("cf_tracking_firefox78")
    public void setCFTrackingFirefox78(String value) { this.cfTrackingFirefox78 = value; }

    @JsonProperty("platform")
    public String getPlatform() { return platform; }
    @JsonProperty("platform")
    public void setPlatform(String value) { this.platform = value; }

    @JsonProperty("cf_crash_signature")
    public String getCFCrashSignature() { return cfCrashSignature; }
    @JsonProperty("cf_crash_signature")
    public void setCFCrashSignature(String value) { this.cfCrashSignature = value; }

    @JsonProperty("cf_status_firefox_esr68")
    public String getCFStatusFirefoxEsr68() { return cfStatusFirefoxEsr68; }
    @JsonProperty("cf_status_firefox_esr68")
    public void setCFStatusFirefoxEsr68(String value) { this.cfStatusFirefoxEsr68 = value; }

    @JsonProperty("regressions")
    public Object[] getRegressions() { return regressions; }
    @JsonProperty("regressions")
    public void setRegressions(Object[] value) { this.regressions = value; }

    @JsonProperty("cf_tracking_firefox_esr68")
    public String getCFTrackingFirefoxEsr68() { return cfTrackingFirefoxEsr68; }
    @JsonProperty("cf_tracking_firefox_esr68")
    public void setCFTrackingFirefoxEsr68(String value) { this.cfTrackingFirefoxEsr68 = value; }

    @JsonProperty("dupe_of")
    public Object getDupeOf() { return dupeOf; }
    @JsonProperty("dupe_of")
    public void setDupeOf(Object value) { this.dupeOf = value; }

    @JsonProperty("cf_status_firefox78")
    public String getCFStatusFirefox78() { return cfStatusFirefox78; }
    @JsonProperty("cf_status_firefox78")
    public void setCFStatusFirefox78(String value) { this.cfStatusFirefox78 = value; }

    @JsonProperty("cf_tracking_firefox76")
    public String getCFTrackingFirefox76() { return cfTrackingFirefox76; }
    @JsonProperty("cf_tracking_firefox76")
    public void setCFTrackingFirefox76(String value) { this.cfTrackingFirefox76 = value; }

    @JsonProperty("cf_has_regression_range")
    public String getCFHasRegressionRange() { return cfHasRegressionRange; }
    @JsonProperty("cf_has_regression_range")
    public void setCFHasRegressionRange(String value) { this.cfHasRegressionRange = value; }

    @JsonProperty("assigned_to")
    public String getAssignedTo() { return assignedTo; }
    @JsonProperty("assigned_to")
    public void setAssignedTo(String value) { this.assignedTo = value; }

    @JsonProperty("type")
    public String getType() { return type; }
    @JsonProperty("type")
    public void setType(String value) { this.type = value; }

    @JsonProperty("groups")
    public Object[] getGroups() { return groups; }
    @JsonProperty("groups")
    public void setGroups(Object[] value) { this.groups = value; }

    @JsonProperty("priority")
    public String getPriority() { return priority; }
    @JsonProperty("priority")
    public void setPriority(String value) { this.priority = value; }

    @JsonProperty("cc")
    public String[] getCc() { return cc; }
    @JsonProperty("cc")
    public void setCc(String[] value) { this.cc = value; }

    @JsonProperty("cf_last_resolved")
    public OffsetDateTime getCFLastResolved() { return cfLastResolved; }
    @JsonProperty("cf_last_resolved")
    public void setCFLastResolved(OffsetDateTime value) { this.cfLastResolved = value; }

    @JsonProperty("cf_rank")
    public Object getCFRank() { return cfRank; }
    @JsonProperty("cf_rank")
    public void setCFRank(Object value) { this.cfRank = value; }

    @JsonProperty("keywords")
    public String[] getKeywords() { return keywords; }
    @JsonProperty("keywords")
    public void setKeywords(String[] value) { this.keywords = value; }

    @JsonProperty("comment_count")
    public long getCommentCount() { return commentCount; }
    @JsonProperty("comment_count")
    public void setCommentCount(long value) { this.commentCount = value; }

    @JsonProperty("severity")
    public String getSeverity() { return severity; }
    @JsonProperty("severity")
    public void setSeverity(String value) { this.severity = value; }

    @JsonProperty("id")
    public long getID() { return id; }
    @JsonProperty("id")
    public void setID(long value) { this.id = value; }

    @JsonProperty("regressed_by")
    public Object[] getRegressedBy() { return regressedBy; }
    @JsonProperty("regressed_by")
    public void setRegressedBy(Object[] value) { this.regressedBy = value; }

    @JsonProperty("is_open")
    public boolean getIsOpen() { return isOpen; }
    @JsonProperty("is_open")
    public void setIsOpen(boolean value) { this.isOpen = value; }

    @JsonProperty("cf_qa_whiteboard")
    public String getCFQAWhiteboard() { return cfQAWhiteboard; }
    @JsonProperty("cf_qa_whiteboard")
    public void setCFQAWhiteboard(String value) { this.cfQAWhiteboard = value; }

    @JsonProperty("cf_tracking_firefox_sumo")
    public String getCFTrackingFirefoxSumo() { return cfTrackingFirefoxSumo; }
    @JsonProperty("cf_tracking_firefox_sumo")
    public void setCFTrackingFirefoxSumo(String value) { this.cfTrackingFirefoxSumo = value; }

    @JsonProperty("depends_on")
    public Object[] getDependsOn() { return dependsOn; }
    @JsonProperty("depends_on")
    public void setDependsOn(Object[] value) { this.dependsOn = value; }

    @JsonProperty("cf_tracking_firefox79")
    public String getCFTrackingFirefox79() { return cfTrackingFirefox79; }
    @JsonProperty("cf_tracking_firefox79")
    public void setCFTrackingFirefox79(String value) { this.cfTrackingFirefox79 = value; }

    @JsonProperty("cf_tracking_firefox77")
    public String getCFTrackingFirefox77() { return cfTrackingFirefox77; }
    @JsonProperty("cf_tracking_firefox77")
    public void setCFTrackingFirefox77(String value) { this.cfTrackingFirefox77 = value; }

    @JsonProperty("status")
    public String getStatus() { return status; }
    @JsonProperty("status")
    public void setStatus(String value) { this.status = value; }

    @JsonProperty("url")
    public String getURL() { return url; }
    @JsonProperty("url")
    public void setURL(String value) { this.url = value; }

    @JsonProperty("target_milestone")
    public String getTargetMilestone() { return targetMilestone; }
    @JsonProperty("target_milestone")
    public void setTargetMilestone(String value) { this.targetMilestone = value; }

    @JsonProperty("qa_contact")
    public String getQAContact() { return qaContact; }
    @JsonProperty("qa_contact")
    public void setQAContact(String value) { this.qaContact = value; }

    @JsonProperty("creator_detail")
    public Detail getCreatorDetail() { return creatorDetail; }
    @JsonProperty("creator_detail")
    public void setCreatorDetail(Detail value) { this.creatorDetail = value; }

    @JsonProperty("cf_has_str")
    public String getCFHasStr() { return cfHasStr; }
    @JsonProperty("cf_has_str")
    public void setCFHasStr(String value) { this.cfHasStr = value; }

    @JsonProperty("creator")
    public String getCreator() { return creator; }
    @JsonProperty("creator")
    public void setCreator(String value) { this.creator = value; }

    @JsonProperty("cf_root_cause")
    public String getCFRootCause() { return cfRootCause; }
    @JsonProperty("cf_root_cause")
    public void setCFRootCause(String value) { this.cfRootCause = value; }

    @JsonProperty("cc_detail")
    public Detail[] getCcDetail() { return ccDetail; }
    @JsonProperty("cc_detail")
    public void setCcDetail(Detail[] value) { this.ccDetail = value; }

    @JsonProperty("cf_status_firefox76")
    public String getCFStatusFirefox76() { return cfStatusFirefox76; }
    @JsonProperty("cf_status_firefox76")
    public void setCFStatusFirefox76(String value) { this.cfStatusFirefox76 = value; }

    @JsonProperty("cf_webcompat_priority")
    public String getCFWebcompatPriority() { return cfWebcompatPriority; }
    @JsonProperty("cf_webcompat_priority")
    public void setCFWebcompatPriority(String value) { this.cfWebcompatPriority = value; }

    @JsonProperty("op_sys")
    public String getOpSys() { return opSys; }
    @JsonProperty("op_sys")
    public void setOpSys(String value) { this.opSys = value; }

    @JsonProperty("cf_fx_points")
    public String getCFFxPoints() { return cfFxPoints; }
    @JsonProperty("cf_fx_points")
    public void setCFFxPoints(String value) { this.cfFxPoints = value; }

    @JsonProperty("resolution")
    public String getResolution() { return resolution; }
    @JsonProperty("resolution")
    public void setResolution(String value) { this.resolution = value; }

    @JsonProperty("see_also")
    public Object[] getSeeAlso() { return seeAlso; }
    @JsonProperty("see_also")
    public void setSeeAlso(Object[] value) { this.seeAlso = value; }

    @JsonProperty("cf_fission_milestone")
    public String getCFFissionMilestone() { return cfFissionMilestone; }
    @JsonProperty("cf_fission_milestone")
    public void setCFFissionMilestone(String value) { this.cfFissionMilestone = value; }

    @JsonProperty("is_creator_accessible")
    public boolean getIsCreatorAccessible() { return isCreatorAccessible; }
    @JsonProperty("is_creator_accessible")
    public void setIsCreatorAccessible(boolean value) { this.isCreatorAccessible = value; }

    @JsonProperty("last_change_time")
    public OffsetDateTime getLastChangeTime() { return lastChangeTime; }
    @JsonProperty("last_change_time")
    public void setLastChangeTime(OffsetDateTime value) { this.lastChangeTime = value; }

    @JsonProperty("cf_status_firefox79")
    public String getCFStatusFirefox79() { return cfStatusFirefox79; }
    @JsonProperty("cf_status_firefox79")
    public void setCFStatusFirefox79(String value) { this.cfStatusFirefox79 = value; }

    @JsonProperty("component")
    public String getComponent() { return component; }
    @JsonProperty("component")
    public void setComponent(String value) { this.component = value; }

    @JsonProperty("product")
    public String getProduct() { return product; }
    @JsonProperty("product")
    public void setProduct(String value) { this.product = value; }

    @JsonProperty("classification")
    public String getClassification() { return classification; }
    @JsonProperty("classification")
    public void setClassification(String value) { this.classification = value; }

    @JsonProperty("cf_user_story")
    public String getCFUserStory() { return cfUserStory; }
    @JsonProperty("cf_user_story")
    public void setCFUserStory(String value) { this.cfUserStory = value; }

    @JsonProperty("votes")
    public long getVotes() { return votes; }
    @JsonProperty("votes")
    public void setVotes(long value) { this.votes = value; }

    @JsonProperty("is_cc_accessible")
    public boolean getIsCcAccessible() { return isCcAccessible; }
    @JsonProperty("is_cc_accessible")
    public void setIsCcAccessible(boolean value) { this.isCcAccessible = value; }

    @JsonProperty("summary")
    public String getSummary() { return summary; }
    @JsonProperty("summary")
    public void setSummary(String value) { this.summary = value; }

    @JsonProperty("version")
    public String getVersion() { return version; }
    @JsonProperty("version")
    public void setVersion(String value) { this.version = value; }

    @JsonProperty("cf_status_firefox77")
    public String getCFStatusFirefox77() { return cfStatusFirefox77; }
    @JsonProperty("cf_status_firefox77")
    public void setCFStatusFirefox77(String value) { this.cfStatusFirefox77 = value; }

    @JsonProperty("is_confirmed")
    public boolean getIsConfirmed() { return isConfirmed; }
    @JsonProperty("is_confirmed")
    public void setIsConfirmed(boolean value) { this.isConfirmed = value; }

    @JsonProperty("alias")
    public Object getAlias() { return alias; }
    @JsonProperty("alias")
    public void setAlias(Object value) { this.alias = value; }

    @JsonProperty("whiteboard")
    public String getWhiteboard() { return whiteboard; }
    @JsonProperty("whiteboard")
    public void setWhiteboard(String value) { this.whiteboard = value; }
}




//
//{
//        "flags": [
//        {
//        "status": "?",
//        "type_id": 800,
//        "setter": "florian@mozilla.com",
//        "creation_date": "2020-03-27T12:35:21Z",
//        "name": "needinfo",
//        "modification_date": "2020-03-27T12:35:21Z",
//        "id": 1960129,
//        "requestee": "gsquelart@mozilla.com"
//        }
//        ],
//        "assigned_to_detail": {
//        "real_name": "Nobody; OK to take it and work on it",
//        "email": "nobody@mozilla.org",
//        "id": 1,
//        "name": "nobody@mozilla.org",
//        "nick": "nobody"
//        },
//        "duplicates": [],
//        "cf_fx_iteration": "---",
//        "blocks": [
//        1414495
//        ],
//        "mentors_detail": [],
//        "creation_time": "2020-03-25T14:38:00Z",
//        "cf_tracking_firefox_relnote": "---",
//        "mentors": [],
//        "cf_tracking_firefox78": "---",
//        "platform": "Unspecified",
//        "cf_crash_signature": "",
//        "cf_status_firefox_esr68": "---",
//        "regressions": [],
//        "cf_tracking_firefox_esr68": "---",
//        "dupe_of": null,
//        "cf_status_firefox78": "disabled",
//        "cf_tracking_firefox76": "---",
//        "cf_has_regression_range": "---",
//        "assigned_to": "nobody@mozilla.org",
//        "type": "defect",
//        "groups": [],
//        "priority": "P1",
//        "cc": [
//        "egao@mozilla.com",
//        "florian@mozilla.com",
//        "gijskruitbosch+bugs@gmail.com",
//        "gsquelart@mozilla.com",
//        "hskupin@gmail.com",
//        "jteh@mozilla.com",
//        "karlt@mozbugz.karlt.net",
//        "mkmelin+mozilla@iki.fi",
//        "mstange@themasta.com"
//        ],
//        "cf_last_resolved": "2020-05-06T21:53:23Z",
//        "cf_rank": null,
//        "keywords": [
//        "intermittent-failure",
//        "leave-open",
//        "test-disabled"
//        ],
//        "comment_count": 37,
//        "severity": "normal",
//        "id": 1624868,
//        "regressed_by": [],
//        "is_open": true,
//        "cf_qa_whiteboard": "",
//        "cf_tracking_firefox_sumo": "---",
//        "depends_on": [],
//        "cf_tracking_firefox79": "---",
//        "cf_tracking_firefox77": "---",
//        "status": "NEW",
//        "url": "",
//        "target_milestone": "---",
//        "qa_contact": "",
//        "creator_detail": {
//        "nick": "gbrown",
//        "id": 411471,
//        "email": "gbrown@mozilla.com",
//        "real_name": "Geoff Brown [:gbrown]",
//        "name": "gbrown@mozilla.com"
//        },
//        "cf_has_str": "---",
//        "creator": "gbrown@mozilla.com",
//        "cf_root_cause": "---",
//        "cc_detail": [
//        {
//        "nick": "egao",
//        "name": "egao@mozilla.com",
//        "email": "egao@mozilla.com",
//        "real_name": "Edwin Takahashi (:egao)",
//        "id": 622974
//        },
//        {
//        "name": "florian@mozilla.com",
//        "id": 149052,
//        "email": "florian@mozilla.com",
//        "real_name": "Florian Quèze [:florian]",
//        "nick": "florian"
//        },
//        {
//        "nick": "Gijs",
//        "name": "gijskruitbosch+bugs@gmail.com",
//        "email": "gijskruitbosch+bugs@gmail.com",
//        "real_name": ":Gijs (he/him)",
//        "id": 159069
//        },
//        {
//        "nick": "gerald",
//        "id": 515575,
//        "real_name": "Gerald Squelart [:gerald] (he/him)",
//        "email": "gsquelart@mozilla.com",
//        "name": "gsquelart@mozilla.com"
//        },
//        {
//        "name": "hskupin@gmail.com",
//        "real_name": "Henrik Skupin (:whimboo) [⌚️UTC+2]",
//        "id": 76551,
//        "email": "hskupin@gmail.com",
//        "nick": "whimboo"
//        },
//        {
//        "name": "jteh@mozilla.com",
//        "real_name": "James Teh [:Jamie]",
//        "id": 219126,
//        "email": "jteh@mozilla.com",
//        "nick": "Jamie"
//        },
//        {
//        "id": 274246,
//        "real_name": "Karl Tomlinson (:karlt)",
//        "email": "karlt@mozbugz.karlt.net",
//        "name": "karlt@mozbugz.karlt.net",
//        "nick": "karlt"
//        },
//        {
//        "nick": "mkmelin",
//        "name": "mkmelin+mozilla@iki.fi",
//        "id": 101158,
//        "email": "mkmelin+mozilla@iki.fi",
//        "real_name": "Magnus Melin [:mkmelin]"
//        },
//        {
//        "nick": "mstange",
//        "email": "mstange@themasta.com",
//        "real_name": "Markus Stange [:mstange]",
//        "id": 293943,
//        "name": "mstange@themasta.com"
//        }
//        ],
//        "cf_status_firefox76": "---",
//        "cf_webcompat_priority": "---",
//        "op_sys": "Unspecified",
//        "cf_fx_points": "---",
//        "resolution": "",
//        "see_also": [],
//        "cf_fission_milestone": "---",
//        "is_creator_accessible": true,
//        "last_change_time": "2020-05-27T05:56:43Z",
//        "cf_status_firefox79": "---",
//        "component": "General",
//        "product": "Firefox",
//        "classification": "Client Software",
//        "cf_user_story": "",
//        "votes": 0,
//        "is_cc_accessible": true,
//        "summary": "Perma linux ccov TEST-UNEXPECTED-TIMEOUT | automation.py | application timed out after 370 seconds with no output  [browser/base/content/test/performance/io]",
//        "version": "unspecified",
//        "cf_status_firefox77": "---",
//        "is_confirmed": true,
//        "alias": null,
//        "whiteboard": ""
//        }