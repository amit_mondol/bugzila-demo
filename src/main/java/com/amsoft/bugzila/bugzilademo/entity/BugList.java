package com.amsoft.bugzila.bugzilademo.entity;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BugList {
    private List<Bug> bugs;

    public BugList() {
    }

    public BugList(List<Bug> bugs) {
        this.bugs = bugs;
    }

    public List<Bug> getBugs() {
        return bugs;
    }

    public void setBugs(List<Bug> bugs) {
        this.bugs = bugs;
    }

    @Override
    public String toString() {
        return "BugList{" +
                "bugs=" + bugs +
                '}';
    }
}
