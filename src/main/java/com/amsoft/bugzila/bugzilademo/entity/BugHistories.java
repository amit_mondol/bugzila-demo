package com.amsoft.bugzila.bugzilademo.entity;

import com.fasterxml.jackson.annotation.*;

import java.util.Arrays;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BugHistories {
    private BugHistory[] bugHistories;

    @JsonProperty("bugs")
    public BugHistory[] getBugHistories() { return bugHistories; }
    @JsonProperty("bugs")
    public void setBugHistories(BugHistory[] value) { this.bugHistories = value; }


    @Override
    public String toString() {
        return "BugHistories{" +
                "bugHistories=" + Arrays.toString(bugHistories) +
                '}';
    }
}