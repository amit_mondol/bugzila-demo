package com.amsoft.bugzila.bugzilademo.entity;

import com.fasterxml.jackson.annotation.*;
import java.time.OffsetDateTime;
import java.util.Arrays;

@JsonIgnoreProperties(ignoreUnknown = true)
public class History {
    private OffsetDateTime when;
    private String who;
    private Change[] changes;

    @JsonProperty("when")
    public OffsetDateTime getWhen() { return when; }
    @JsonProperty("when")
    public void setWhen(OffsetDateTime value) { this.when = value; }

    @JsonProperty("who")
    public String getWho() { return who; }
    @JsonProperty("who")
    public void setWho(String value) { this.who = value; }

    @JsonProperty("changes")
    public Change[] getChanges() { return changes; }
    @JsonProperty("changes")
    public void setChanges(Change[] value) { this.changes = value; }


    @Override
    public String toString() {
        return "History{" +
                "when=" + when +
                ", who='" + who + '\'' +
                ", changes=" + Arrays.toString(changes) +
                '}';
    }
}
