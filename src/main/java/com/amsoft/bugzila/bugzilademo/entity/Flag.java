package com.amsoft.bugzila.bugzilademo.entity;

import com.fasterxml.jackson.annotation.*;
import java.time.OffsetDateTime;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Flag {
    private String status;
    private long typeID;
    private String setter;
    private OffsetDateTime creationDate;
    private String name;
    private OffsetDateTime modificationDate;
    private long id;
    private String requestee;

    @JsonProperty("status")
    public String getStatus() { return status; }
    @JsonProperty("status")
    public void setStatus(String value) { this.status = value; }

    @JsonProperty("type_id")
    public long getTypeID() { return typeID; }
    @JsonProperty("type_id")
    public void setTypeID(long value) { this.typeID = value; }

    @JsonProperty("setter")
    public String getSetter() { return setter; }
    @JsonProperty("setter")
    public void setSetter(String value) { this.setter = value; }

    @JsonProperty("creation_date")
    public OffsetDateTime getCreationDate() { return creationDate; }
    @JsonProperty("creation_date")
    public void setCreationDate(OffsetDateTime value) { this.creationDate = value; }

    @JsonProperty("name")
    public String getName() { return name; }
    @JsonProperty("name")
    public void setName(String value) { this.name = value; }

    @JsonProperty("modification_date")
    public OffsetDateTime getModificationDate() { return modificationDate; }
    @JsonProperty("modification_date")
    public void setModificationDate(OffsetDateTime value) { this.modificationDate = value; }

    @JsonProperty("id")
    public long getID() { return id; }
    @JsonProperty("id")
    public void setID(long value) { this.id = value; }

    @JsonProperty("requestee")
    public String getRequestee() { return requestee; }
    @JsonProperty("requestee")
    public void setRequestee(String value) { this.requestee = value; }
}
