package com.amsoft.bugzila.bugzilademo.entity;

import com.fasterxml.jackson.annotation.*;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Detail {
    private String realName;
    private String email;
    private long id;
    private String name;
    private String nick;

    @JsonProperty("real_name")
    public String getRealName() { return realName; }
    @JsonProperty("real_name")
    public void setRealName(String value) { this.realName = value; }

    @JsonProperty("email")
    public String getEmail() { return email; }
    @JsonProperty("email")
    public void setEmail(String value) { this.email = value; }

    @JsonProperty("id")
    public long getID() { return id; }
    @JsonProperty("id")
    public void setID(long value) { this.id = value; }

    @JsonProperty("name")
    public String getName() { return name; }
    @JsonProperty("name")
    public void setName(String value) { this.name = value; }

    @JsonProperty("nick")
    public String getNick() { return nick; }
    @JsonProperty("nick")
    public void setNick(String value) { this.nick = value; }
}