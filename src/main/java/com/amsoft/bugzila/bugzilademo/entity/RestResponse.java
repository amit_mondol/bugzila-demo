package com.amsoft.bugzila.bugzilademo.entity;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RestResponse {

    private List<Bug> bugs;

    public RestResponse() {
    }

    public List<Bug> getBugs() {
        return bugs;
    }

    public void setBugs(List<Bug> bugs) {
        this.bugs = bugs;
    }

    @Override
    public String toString() {
        return "RestResponse{" +
                "bugs=" + bugs +
                '}';
    }
}